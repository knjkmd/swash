% plotjes tbv partiele reflectie en transmissie (Hm0)
%
fignr=2;
load l42por02.tab
load l42por05.tab
plot(l42por02(:,1),100*l42por02(:,2),'k','LineWidth',1); hold on
plot(l42por05(:,1),100*l42por05(:,2),'r','LineWidth',1)
xlabel('distance [m]')
ylabel('H_{m0} [cm]');
axis([0 27 0 15])
legend('kmax = 1','kmax = 2')
set(gca,'PlotBoxAspectRatio',[2 1 1]);
eval(['print -dpng tra' num2str(fignr) 'b.png']);
