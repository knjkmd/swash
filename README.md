# swash
This is a repository to keep track of changes of the SWASH project.

https://www.tudelft.nl/en/ceg/about-faculty/departments/hydraulic-engineering/sections/environmental-fluid-mechanics/research/swash/

Version 6.01 was downloaded from source forge to start with this repo.

## Test cases
There are official test cases and they are shown in the table below. Computation time was measured with my laptop with the following spec:
* core i5 5300U 2.30GHz
* memory 12 GB
  
It is just a mesurement for you not to wait and feel unexpectedly long.


| Test case title | Description | Comp. time (sec)
| --- | --- | --- |
 a11stwav |  a test with standing waves in a basin | 0.050 | 
 a13stwav |  more tests with standing waves in a basin | 0.143 |
 a14prwav |  tests with progressive waves | 4.289 |
 a31wndcb |  tests with wind setup in closed basin | 0.070 |
 a34wndco |  tests with wind setup in semi-closed basin | 0.054 | 
 a46jump  |  test with hydraulic jump and drop in open river | 0.088 | 
 a61dambr |  tests with dam break | 2.000 |
 a81tidal |  tidal wave flow case | 0.068 |
 l12bbbar |  test with wave propagation over submerged bar (Beji and Battjes experiment) | 18.365 |
 l31setup |  test with wave breaking on barred beach (Boers 1C experiment) | 532.920 |
 l33turb  |  2DV turbulent channel flow (Nezu and Rodi, 1986) | 0.436 | 
 l41berkh |  wave deformation by an elliptic shoal on sloped bottom (Berkhoff et al. experiment) | 698.169 | 
 l42poros |  test with partial reflection and transmission | 50.532
 t11coupl |  test of coupling SWAN - SWASH | 213.523 | 
 t52lock  |  lock exchange test | 2.575 | 