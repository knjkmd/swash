#!/bin/bash
for testcase_dir in $(ls -d ./testcases/*/)
do 
#   echo "Testcase directory: $testcase_dir"
#   cp ./src/swashrun "$testcase_dir"
#   cp ./src/swash.exe "$testcase_dir"
  cd "$testcase_dir"
  for sws_name in $(find . -name '*.sws')
  do
    sws_namee=$(basename $sws_name)
   #  echo "sws_name: $sws_namee"
    ts=$(date +%s%N)
    bash swashrun -input "$sws_name" >/dev/null 2>&1 
    echo "Testcase $sws_namee took $((($(date +%s%N) - $ts)/1000000)) mili seconds."
  done
  cd ../..  
done

find ./testcases -name swashrun -exec rm {} +
find ./testcases -name swash.exe -exec rm {} +
