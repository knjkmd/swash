% plotjes tbv partiele reflectie en transmissie (Hm0)
%
fignr=1;
load l42por01.tab
load l42por04.tab
plot(l42por01(:,1),100*l42por01(:,2),'k','LineWidth',1); hold on
plot(l42por04(:,1),100*l42por04(:,2),'r','LineWidth',1)
xlabel('distance [m]')
ylabel('H_{m0} [cm]');
axis([0 27 0 15])
legend('kmax = 1','kmax = 2')
set(gca,'PlotBoxAspectRatio',[2 1 1]);
eval(['print -dpng tra' num2str(fignr) 'b.png']);
