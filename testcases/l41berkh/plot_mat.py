import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat

x = loadmat('l41ber01.mat')
plt.figure()

key = 'Watlev_000030_000'
data = x[key]
print(key)        
plt.matshow(data)
plt.title(key)
