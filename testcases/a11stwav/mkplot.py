#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

exact = np.loadtxt("exact.dat")
a11stw01 = np.loadtxt("a11stw01.tbl")

t = a11stw01[:,0]
wl = a11stw01[:,1]

plt.plot(t, wl, 'k')
plt.plot(exact[0:601:4,0], exact[0:601:4,1], 'ko', fillstyle='none')

plt.xlabel('time [s]');
plt.ylabel('water level [m]');
plt.title(r'kh=0.55$\pi$; depth averaged')
plt.tight_layout()
plt.grid(True)
plt.savefig('seiche1_py.png')