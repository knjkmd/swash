% plotjes tbv partiele reflectie en transmissie
%
clear wlv
fignr=2;
%--------------------------------------
TMP = swash_loadTableData('l42por02.tbl',271,3);
x   = TMP{3}(1,:);
wlv = TMP{4};
wlv(wlv==-99)=NaN;
nt  = length(TMP{2});
%
sampl  = 20  ; % sampling in Hz
period = 1.86; % number of wave periods in s
is     = nt-floor(sampl*period);
%
%------------------------
%
for i=is:nt
    data=wlv(i,:);
    plot(x,data)
    hold on
end
xlabel('distance [m]')
ylabel('\zeta [m]');
axis([0 27 -0.06 0.08])
set(gca,'PlotBoxAspectRatio',[2 1 1]);
eval(['print -dpng tra' num2str(fignr) 'a.png']);
